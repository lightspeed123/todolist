# ARTEMEON Management Partner GmbH

## **1  AUFGABE**

### **1.1 EINFÜHRUNG**

Willkommen bei der ARTEMEON Coding-Challenge. Der Sinn dieser Aufgabe ist, dass wir deine Programmierkenntnisse besser kennenlernen. Es gibt hier keine richtige Lösung, wir wollen nur einen ersten Eindruck von deinen Fähigkeiten bekommen. Von daher löse die Aufgabe am besten so wie du es für richtig hältst (mit den Tools die du am besten kennst). Natürlich finden wir es super, wenn bestehender Code wiederverwendet wird. Für uns ist es nur hilfreich, wenn wir einfach erkennen können welcher Code von dir ist.

### **1.2 ZIEL**

Ziel ist es, eine kleine Todo-App als Web-Anwendung zu entwickeln. Ein Benutzer hat dort die Möglichkeit Todo Einträge anzusehen, anzulegen und vorhandene Einträge als erledigt zu markieren. Die Einträge sollen über ein PHP Backend in einer Datenbank (z.B. MySQL oder SQlite) gespeichert werden. Uns ist durchaus bewusst, dass man in solchen Fällen natürlich auf fertige Libraries und Frameworks zu setzt. Da wir ja aber dich und deine Coding Skills kennenlernen möchten wäre es super, wenn ein wenig mehr direkt von dir entwickelt wird. Für das Frontend hast du die freie Auswahl, du kannst z.B. ein einfaches HTML Formular, Bootstrap + JQuery oder ein modernes JavaScript Framework benutzen. Es geht dabei nicht darum, das allerschönste User Interface zu bauen, sondern die Funktionalität zeigen zu können. Es geht somit nicht um das perfekt Button Styling oder die schickste JS-Animation. Solltest du weitere Frage haben kannst du uns natürlich jederzeit ansprechen.

### **1.3 ERGEBNIS**

Das Ergebnis sollte eine einfache .zip Datei sein. Diese beinhaltet dann alle .php und sonstige Dateien (HTML, JavaScript, Datenbank-Schema, etc.) damit wir die Todo-App lokal testen können. Sollte das Setup komplizierter sein füge am besten eine kurze README Datei hinzu in der die Installation beschrieben ist.

## **2 Installation**

Für eine geräteunabhängige Nutzung, habe ich das Projekt in ddev, einem Docker Wrapper, eingebunden. Wie man ddev installiert (Docker wird vorausgesetzt), steht auf **[dieser Seite](https://ddev.readthedocs.io/en/stable/)**. Nachdem ddev installiert wurde, kann man den Server einfach mit `ddev start` starten. Es wird ein Server mit nginx und PHP 7.2 aufgesetzt. 

Die Webseite kann unter http://todolist.ddev.local/ aufgerufen werden. 
Um weitere Einrichtungen muss sich nicht gekümmert werden. 

## **3 Bedienung**
Die TodoList lässt sich recht einfach bedienen. 
In das Inputfeld gibt man die Aufgabe ein, den man erledigen möchte. 
Ist man mit der Aufgabe fertig, klickt man auf die Checkbox, um sie als erledigt zu makieren.

Da in der Zielsetzung nicht geschrieben steht, dass die Aufgabe auch als "nicht erledigt" gesetzt werden soll, oder gelöscht werden soll, habe ich diese Fälle nicht eingebunden, 


## **4 Disclaimer**

Es tut mir leid, dass es doch so lange gedauert hat, bis die Aufgabe fertig wurde, aber in der ersten Woche war ich krank und in dieser Woche war privat und beruflich viel zu tun, wodurch das Projekt hinten anstehen musste. 

Ich hoffe, das wirkt sich nicht negativ auf das Gesamtergebnis aus. 

Mit besten Grüßen.