let result = [];
let input = document.querySelector('#text-input');
let does = document.querySelector("#does");
let done = document.querySelector("#done");
let checkboxes = document.querySelectorAll(".statusButton ");
let statusClasses = ['started', 'finshed'];

window.onload = () => {
    result = getList();
};

async function getList() {
    result = await callServer("GET", "get");
    console.log(result);
    result.forEach((e) => {
        let node = document.createElement("LI");
        let textnode = document.createTextNode(e.task);
        node.setAttribute('id', e.id);
        node.setAttribute('data-status', e.status);
        switch (e.status) {
            case "0":
                does.appendChild(node);
                break;
            case "1":
                done.appendChild(node);
                break;
        }
        setStatusClass(node, e.status);
        node.append(textnode);
    });
}

function addToList(task) {
    result = callServer("POST", "add", task);
    let node = document.createElement("LI");
    console.log(result);
    result.then((e) => {
        node.setAttribute('id', e);
        setStatusClass(node, e.status)
    });
    let textnode = document.createTextNode(input.value);
    let finishedButton = document.createElement("INPUT");
    finishedButton.setAttribute("type", "checkbox");
    finishedButton.addEventListener('input', (e) => {
        done.appendChild(node);
        node.removeChild(finishedButton);
    });
    finishedButton.classList.add("statusButton", "finished");

    does.appendChild(node);
    node.prepend(finishedButton);
    node.appendChild(textnode);
}

function setStatusClass(node, status) {
    node.classList.add(statusClasses[status]);
    let finishedButton = document.createElement("INPUT");
    finishedButton.setAttribute("type", "checkbox");
    finishedButton.addEventListener('input', (e) => {
        callServer('POST', 'update', '',1, node.valueOf().id);
        done.appendChild(node);
        node.removeChild(finishedButton);
    });
    finishedButton.classList.add("statusButton", "finished");

    switch (Number(status)) {
        case 0:
            node.prepend(finishedButton);
            break;
        case 1:
            finishedButton.classList.add("active");
            break;
    }
}

async function callServer(method, action, task = "", status = 0, id = null) {
    response = await fetch("src.php?action=" + action + "&id=" + id + "&task=" + task + "&status=" + status,
        {
            method: method,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    return await response.json();
}

input.addEventListener('keyup', (e) => {
    if ((e.code === "Enter") || (e.code === "NumpadEnter")) {
        addToList(input.value);
        input.value = "";
    }
});