<?php
include_once "../vendor/autoload.php";

use Isik\Todolist\Domain\Repository\ListRepository;

$vars = $_POST;

$listRepo = new ListRepository();

$action = $_REQUEST['action'];
$id = $_REQUEST['id'];
$task = $_REQUEST['task'];
$status = $_REQUEST['status'];

switch ($action) {

    case "get":
        $result = $listRepo->getList();
        die(json_encode($result));
        break;

    case "add":
        $result = $listRepo->addListItem($task);
        die(json_encode($result));
        break;

    case "update":
        $result = $listRepo->updateStatus($id, $status);
        die(json_encode($result));
        break;
}