<?php
namespace Isik\Todolist\Domain\Repository;

use Isik\Todolist\Controller\ListController;

class ListRepository
{

    /**
     * @var ListController $listController
     */
    protected $listController;

    protected $result;

    public function __construct()
    {
        $this->listController = new ListController();
    }

    public function getList()
    {
        $this->result = $this->listController->getList();
        return $this->result;
    }

    public function addListItem($task)
    {
        $this->result = $this->listController->addItem($task);
        return $this->result;
    }

    public function updateStatus($id, $status)
    {
        $this->result = $this->listController->updateStatus($id, $status);
        return $this->result;
    }
}