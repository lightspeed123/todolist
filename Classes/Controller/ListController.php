<?php

namespace Isik\Todolist\Controller;

use PDO;

class ListController
{
    /**
     * @var PDO $databaseHandler ;
     */
    protected $databaseHandler;

    public function __construct()
    {
        $dsn = 'mysql:dbname=db;host=db;port=3306';
        $user = 'db';
        $password = 'db';
        try {
            $this->databaseHandler = new PDO($dsn, $user, $password);
            $this->databaseHandler->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

        } catch (\Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    public function getList()
    {
        $result = $this->databaseHandler->query('SELECT * FROM item')->fetchAll();
        return $result;
    }

    public function addItem($task)
    {
        $this->databaseHandler->query("INSERT INTO item (task, status) VALUES (\"{$task}\", 0) ");
        return $this->databaseHandler->lastInsertId();
    }

    public function updateStatus($id, $status)
    {
        $result = $this->databaseHandler->query("UPDATE item SET status = {$status} WHERE id = {$id}");
        return $result;
    }


}